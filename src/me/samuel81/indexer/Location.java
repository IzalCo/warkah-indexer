package me.samuel81.indexer;

public enum Location {

	CILINCING(
			new String[] { "CILINCING", "KALIBARU", "MARUNDA", "ROROTAN", "SEMPER BARAT", "SEMPER TIMUR", "SUKAPURA" }),
	KELAPA_GADING(new String[] { "KELAPA GADING BARAT", "KELAPA GADING TIMUR", "PEGANGSAAN DUA" }),
	PENJARINGAN(new String[] { "KAMAL MUARA", "KAPUK MUARA", "PEJAGALAN", "PENJARINGAN", "PLUIT" });

	String[] kelurahan;

	Location(String[] kel) {
		kelurahan = kel;
	}

	public String[] getKelurahan() {
		return kelurahan;
	}

	public static String[] getKecamatan() {
		return new String[] { "CILINCING", "KELAPA GADING", "PENJARINGAN" };
	}

}
