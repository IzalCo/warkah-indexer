package me.samuel81.indexer;

import java.io.File;

import javax.swing.JFrame;

import me.samuel81.indexer.gui.MyFrame;

public class Main {

	private static File templateBT, templateSU;

	public static void main(String... args) {

		initTemplate();

		MyFrame f = new MyFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);

	}

	/**
	 * Load excel template from local file to memory
	 */
	private static void initTemplate() {
		templateBT = new File("template/TBT.ods");
		templateSU = new File("template/TSU.ods");
	}

	public static File getTemplateBT() {
		return templateBT;
	}

	public static File getTemplateSU() {
		return templateSU;
	}

}
