package me.samuel81.indexer.gui;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

import me.samuel81.indexer.FolderType;
import me.samuel81.indexer.Location;
import me.samuel81.indexer.Main;
import me.samuel81.indexer.PDFHandler;
import me.samuel81.indexer.Util;

public class MyFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2062371638919874756L;

	private JFileChooser chooser;

	private JButton btnSource = new JButton("Browse");
	private JButton btnBT = new JButton("Browse");
	private JButton btnSU = new JButton("Browse");
	private JButton pdfSource = new JButton("Browse");
	private JButton startA = new JButton("Start");
	private JButton startB = new JButton("Start");

	private JTextField txtA = new JTextField();
	private JTextField txtB = new JTextField();
	private JTextField txtC = new JTextField();
	private JTextField txtD = new JTextField();

	private JLabel lblA = new JLabel("Source Folder");
	private JLabel lblB = new JLabel("BT Target Folder");
	private JLabel lblC = new JLabel("SU Target Folder");
	private JLabel lblD = new JLabel("Kecamatan");
	private JLabel lblE = new JLabel("Kelurahan");
	private JLabel lblF = new JLabel("Select Action List");
	private JLabel lblG = new JLabel("PDF Source");

	private JComboBox<Location> listKecamatan = new JComboBox<Location>(Location.values());
	private JComboBox<String> listKelurahan = new JComboBox<String>(Location.KELAPA_GADING.getKelurahan());

	private JCheckBox move = new JCheckBox("Move");
	private JCheckBox rename = new JCheckBox("Rename");
	private JCheckBox index = new JCheckBox("Index");
	private JCheckBox purge = new JCheckBox("Purge");

	private JCheckBox rotate = new JCheckBox("Rotate");
	private JCheckBox rbp = new JCheckBox("Remove BP");
	private JCheckBox rearrange = new JCheckBox("Rearrange");

	private File[] csFolder = null;
	private File btTarget, suTarget;

	private File[] pdfFiles = null;

	private String kecamatan = "KELAPA GADING";
	private String kelurahan = "KELAPA GADING BARAT";

	public MyFrame() {
		chooser = new JFileChooser();
		chooser.setCurrentDirectory(new java.io.File("."));

		setTitle("Hi-Indexer");
		setSize(800, 380);
		setLocation(new Point(300, 200));
		setLayout(null);
		setResizable(false);

		initComponent();
		initEvent();
	}

	private void initComponent() {
		btnSource.setBounds(360, 10, 20, 20);
		btnBT.setBounds(360, 40, 20, 20);
		btnSU.setBounds(360, 70, 20, 20);
		startA.setBounds(150, 200, 100, 100);
		startB.setBounds(580, 45, 100, 50);
		pdfSource.setBounds(740, 10, 20, 20);

		txtA.setBounds(150, 10, 200, 20);
		txtA.setEditable(false);
		txtB.setBounds(150, 40, 200, 20);
		txtB.setEditable(false);
		txtC.setBounds(150, 70, 200, 20);
		txtC.setEditable(false);
		txtD.setBounds(530, 10, 200, 20);
		txtD.setEditable(false);

		listKecamatan.setBounds(150, 120, 200, 20);
		listKecamatan.setSelectedItem(Location.KELAPA_GADING);
		listKelurahan.setBounds(150, 150, 200, 20);

		lblA.setBounds(20, 10, 100, 20);
		lblB.setBounds(20, 40, 100, 20);
		lblC.setBounds(20, 70, 100, 20);
		lblD.setBounds(20, 120, 100, 20);
		lblE.setBounds(20, 150, 100, 20);
		lblF.setBounds(20, 200, 100, 20);
		lblG.setBounds(450, 10, 100, 20);

		move.setBounds(15, 220, 100, 20);
		rename.setBounds(15, 240, 100, 20);
		index.setBounds(15, 260, 100, 20);
		purge.setBounds(15, 280, 100, 20);

		rotate.setBounds(445, 35, 100, 20);
		rbp.setBounds(445, 55, 100, 20);
		rearrange.setBounds(445, 75, 100, 20);

		add(btnSource);
		add(btnBT);
		add(btnSU);
		add(startA);
		add(startB);
		add(pdfSource);

		add(lblA);
		add(lblB);
		add(lblC);
		add(lblD);
		add(lblE);
		add(lblF);
		add(lblG);

		add(txtA);
		add(txtB);
		add(txtC);
		add(txtD);

		add(listKecamatan);
		add(listKelurahan);

		add(move);
		add(rename);
		add(index);
		add(purge);
		add(rotate);
		add(rbp);
		add(rearrange);
	}

	private void initEvent() {

		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});

		pdfSource.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				browse(FolderType.PDF_FILES);
			}
		});

		btnSource.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				browse(FolderType.SOURCE);
			}
		});

		btnBT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				browse(FolderType.BT);
			}
		});

		btnSU.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				browse(FolderType.SU);
			}
		});

		startA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!canProcess()) {
					JOptionPane.showMessageDialog(null, "Please setup things first!", "ERROR",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				try {
					process();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});

		startB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (pdfFiles == null || pdfFiles.length < 1) {
					JOptionPane.showMessageDialog(null, "Please select PDF file first!", "ERROR",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				for (File file : pdfFiles) {
					PDFHandler.rotatePDF(file, rotate.isSelected(), rbp.isSelected(), rearrange.isSelected());
				}
				setCursor(null);
				Toolkit.getDefaultToolkit().beep();
				JOptionPane.showMessageDialog(null, "Please do some final QC!", "Done!",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});

		listKecamatan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox<?> cb = (JComboBox<?>) e.getSource();
				Location ke = (Location) cb.getSelectedItem();
				kecamatan = ke.toString().replace("_", " ").toUpperCase();
				listKelurahan.removeAllItems();
				for (String str : ke.getKelurahan()) {
					listKelurahan.addItem(str);
				}
			}
		});

		listKelurahan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox<?> cb = (JComboBox<?>) e.getSource();
				if (cb.getSelectedItem() == null) {
					kelurahan = "";
					return;
				}
				String ke = (String) cb.getSelectedItem();
				kelurahan = ke.toUpperCase();
			}
		});
	}

	public boolean canProcess() {
		return (csFolder != null && btTarget != null && suTarget != null && !kecamatan.equalsIgnoreCase("")
				&& !kelurahan.equalsIgnoreCase("")) && (move.isSelected() || rename.isSelected() || index.isSelected());
	}

	/**
	 * Process all data in the folder also make an excel output from processed data
	 * 
	 * @throws IOException
	 */
	public void process() throws IOException {
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		File btT = new File(btTarget + File.separator + kecamatan + File.separator + kelurahan);
		if (!btT.exists())
			btT.mkdirs();

		File suT = new File(suTarget + File.separator + kecamatan + File.separator + kelurahan);
		if (!suT.exists())
			suT.mkdirs();

		int processedFolder = 0, processedBT = 0, processedSU = 0;

		for (File file : csFolder) {
			String[] fName = file.getName().split("_");
			String warkahType = fName[0].split("\\s+")[0];

			File bt = null;
			File su = null;
			File backup = new File(file + File.separator + "BACKUP TIFF");
			if (!backup.exists())
				backup.mkdirs();

			for (File f : file.listFiles()) {
				if (!f.isDirectory()) {
					if (purge.isSelected())
						f.delete();
					continue;
				}
				if (f.getName().equalsIgnoreCase("BUKU TANAH") || f.getName().equalsIgnoreCase("BT")) {
					bt = f;
				} else if (f.getName().equalsIgnoreCase("SURAT UKUR") || f.getName().equalsIgnoreCase("SU")) {
					su = f;
				}
			}

			File btt1 = new File(btT + File.separator + fName[0]);
			if (!btt1.exists())
				btt1.mkdirs();
			File sutt1 = new File(suT + File.separator + fName[0]);
			if (!sutt1.exists())
				sutt1.mkdirs();

			File btOutput = null;
			File suOutput = null;

			Sheet bts = SpreadSheet.createFromFile(Main.getTemplateBT()).getSheet(0);
			Sheet sus = SpreadSheet.createFromFile(Main.getTemplateSU()).getSheet(0);

			if (index.isSelected() && bt != null && su != null) {
				btOutput = new File(btt1, fName[0] + ".ods");
				suOutput = new File(sutt1, fName[0] + ".ods");
			}

			if (bt != null) {
				int btI = 0;
				bt: for (File btf : bt.listFiles()) {
					if (!Util.pdfSuffixFilter.accept(btf)) {
						btf.renameTo(new File(backup, btf.getName()));
						continue bt;
					}
					String fname = btf.getName();
					if (rename.isSelected()) {
						if (fname.replace(".pdf", "").matches("^\\d{1,5}-\\d{4}")) {
							String newName = warkahType + "_";
							String[] name = fname.replace("-", "_").split("_");
							if (name[0].length() != 5) {
								name[0] = Util.fill('0', 5 - name[0].length()) + name[0];
							}
							newName += name[0] + "_" + name[1];
							btf.renameTo(new File(btf.getParent(), newName));
							fname = newName;
						} else {
							continue bt;
						}
					}

					if (index.isEnabled()) {
						if (!fname.replace(".pdf", "").matches("^" + warkahType + "_{1}\\d{5}_{1}\\d{4}"))
							continue bt;
						String[] ss = fname.split("_");
						int no = btI + 1;
						String noBT = ss[1];
						String tahun = ss[2];
						bts.getCellAt("A" + (5 + btI)).setValue(no);
						bts.getCellAt("B" + (5 + btI)).setValue(noBT);
						bts.getCellAt("C" + (5 + btI)).setValue(kecamatan);
						bts.getCellAt("D" + (5 + btI)).setValue(kelurahan);
						bts.getCellAt("E" + (5 + btI)).setValue(tahun.replace(".pdf", ""));
						bts.getCellAt("F" + (5 + btI)).setValue(fName[0]);
						btI++;
					}
					processedBT++;
				}
			}

			if (su != null) {
				int suI = 0;
				su: for (File btf : su.listFiles()) {
					if (!Util.pdfSuffixFilter.accept(btf))
						continue su;
					String fname = btf.getName();
					if (rename.isSelected()) {
						if (fname.replace(".pdf", "").matches("^\\d{1,5}-\\d{4}")) {
							String newName = "SU_";
							String[] name = btf.getName().replace("-", "_").split("_");
							if (name[0].length() != 5) {
								name[0] = Util.fill('0', 5 - name[0].length()) + name[0];
							}
							newName += name[0] + "_" + name[1];
							btf.renameTo(new File(btf.getParent(), newName));
							fname = newName;
						} else {
							continue su;
						}
					}

					if (index.isEnabled()) {
						if (!fname.replace(".pdf", "").matches("^SU_{1}\\d{5}_{1}\\d{4}"))
							continue su;
						String[] ss = fname.split("_");
						int no = suI + 1;
						String nosu = ss[1];
						String tahun = ss[2];
						sus.getCellAt("A" + (5 + suI)).setValue(no);
						sus.getCellAt("B" + (5 + suI)).setValue(nosu);
						sus.getCellAt("C" + (5 + suI)).setValue(kecamatan);
						sus.getCellAt("D" + (5 + suI)).setValue(kelurahan);
						sus.getCellAt("E" + (5 + suI)).setValue(tahun.replace(".pdf", ""));
						sus.getCellAt("F" + (5 + suI)).setValue(fName[0]);
						suI++;
					}
					processedSU++;
				}
			}

			if (index.isSelected() && bt != null && su != null) {
				bts.getSpreadSheet().saveAs(btOutput);
				sus.getSpreadSheet().saveAs(suOutput);
			}

			/**
			 * Moving all folder to target folder and renaming it
			 */
			if (move.isSelected() && bt != null && su != null) {
				Util.moveFolder(bt, btt1);
				Util.moveFolder(su, sutt1);
			}
			processedFolder++;
		}
		setCursor(null);
		Toolkit.getDefaultToolkit().beep();
		JOptionPane
				.showMessageDialog(null,
						"Please do some final QC!\nProcessed Folder: " + processedFolder + "\nProcessed BT: "
								+ processedBT + "\nProcessed SU: " + processedSU,
						"Done!", JOptionPane.INFORMATION_MESSAGE);

	}

	private void browse(FolderType type) {

		chooser.setDialogTitle(type.getTitle());
		if (type != FolderType.PDF_FILES) {
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			chooser.setAcceptAllFileFilterUsed(false);
		} else {
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setAcceptAllFileFilterUsed(true);
			chooser.setFileFilter(new FileFilter() {

				@Override
				public boolean accept(File file) {
					if (file.isDirectory()) {
						return true;
					} else {
						String path = file.getAbsolutePath().toLowerCase();
						String extension = "pdf";
						if ((path.endsWith(extension)
								&& (path.charAt(path.length() - extension.length() - 1)) == '.')) {
							return true;
						}
					}
					return false;
				}

				@Override
				public String getDescription() {
					return "";
				}
			});
		}
		if (type == FolderType.SOURCE || type == FolderType.PDF_FILES) {
			chooser.setMultiSelectionEnabled(true);
		} else {
			chooser.setMultiSelectionEnabled(false);
		}

		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			if (type == FolderType.SOURCE) {
				csFolder = chooser.getSelectedFiles();
				txtA.setText("Selected " + csFolder.length + " folder(s)");
			} else if (type == FolderType.BT) {
				txtB.setText(chooser.getSelectedFile().getAbsolutePath());
				btTarget = chooser.getSelectedFile();
			} else if (type == FolderType.SU) {
				txtC.setText(chooser.getSelectedFile().getAbsolutePath());
				suTarget = chooser.getSelectedFile();
			} else if (type == FolderType.PDF_FILES) {
				pdfFiles = chooser.getSelectedFiles();
				txtD.setText("Selected " + pdfFiles.length + " pdf file(s)");
			}
		}
	}

}