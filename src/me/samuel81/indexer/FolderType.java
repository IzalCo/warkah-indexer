package me.samuel81.indexer;

public enum FolderType {

	/**
	 * Used for folder/file explorer settings
	 */
	SOURCE("Source Folder"), BT("BT Folder"), SU("SU Folder"), PDF_FILES("PDF Files");

	String title;

	FolderType(String s) {
		title = s;
	}

	public String getTitle() {
		return title;
	}
}
