package me.samuel81.indexer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

public class PDFHandler {

	public static void rotatePDF(File f, boolean autoRotate, boolean autoRBP, boolean autoRearrange) {
		try (InputStream resource = new FileInputStream(f)) {
			PDDocument document = PDDocument.load(resource);
			Iterator<PDPage> iter = document.getPages().iterator();
			int pages = 0;
			int warkah = 0;
			while (iter.hasNext()) {
				PDPage page = iter.next();
				float w = page.getArtBox().getWidth();
				float h = page.getArtBox().getHeight();
				boolean landscape = w > h;
				if (landscape && autoRotate) {
					page.setRotation(90);
				}
				/*
				 * System.out.println("========="); System.out.println(w);
				 * System.out.println(h); System.out.println("=========");
				 */

				if ((w < 700f && h < 500f) && autoRBP) {
					document.removePage(page);
					continue;
				}

				pages++;
				if (pages % 4 == 0)
					warkah++;
			}

			for (int i = 0; i < warkah; i++) {
				if (autoRotate) {
					document.getPage(2 + (i * 4)).setRotation(270);
					document.getPage(3 + (i * 4)).setRotation(270);
				}
				if (autoRearrange) {
					PDPage page = document.getPage(1 + (i * 4));
					document.removePage(1 + (i * 4));
					document.getPages().insertAfter(page, document.getPage(2 + (i * 4)));
				}
			}
			document.save(f);
			document.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
